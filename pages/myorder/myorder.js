const app=getApp()
const api = require("../../api/index.js")
Component({
  data: {
    order:[],
  },
  pageLifetimes: {
    show: function() {
      api.getMyOrder({
        success:(res)=>{
          res.data.data.allOrder.map((item)=>{
            item.zhanshou=false
          })
          this.setData({
            order:res.data.data.allOrder.reverse()
          })
        }
      })
    },
  }
})