
Component({
  data: {
    value:"",
    title:"",
    placeholder:"",
    comfirm:"",
  },
  ready(){
    const eventChannel = this.getOpenerEventChannel()
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel.on('acceptDataFromOpenerPage', (data) => {
      if(data.value==="请填写口味偏好"){
        data.value=""
      }
      this.setData({
        title: data.title,
        placeholder: data.placeholder,
        comfirm:data.comfirm,
        value:data.value
      })
    })
  },
  methods:{
    tijiao(){
      if(this.data.value!=''){
        if(this.data.title==="意见建议"){
          wx.showToast({
            title: '感谢您的建议',
            icon: 'success',
            duration: 2000
          })
        }else if(this.data.title==="备注"){
          wx.navigateBack()
          const eventChannel = this.getOpenerEventChannel()
          eventChannel.emit('beizhu', {
            text:this.data.value
          });
        }
        this.setData({
          value:''
        })
      }
    },
  }
})