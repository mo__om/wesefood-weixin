const api = require("../../api/index.js")
Page({
  data: {
    //刚开始的名字和头像
    name: '点击授权登录',
    avatarUrl: '../../assets/huise.png'
  },
  onShareAppMessage(){},
  onShareTimeline(){},
  onAddToFavorites(){},
  // login方法
  login() {
    if (this.data.name == "点击授权登录") {
      wx.getUserProfile({
        desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          api.login({
            data: {
              nickName: res.userInfo.nickName,
              avatarUrl: res.userInfo.avatarUrl
            },
            success: () => {
              this.setData({
                name: res.userInfo.nickName,
                avatarUrl: res.userInfo.avatarUrl
              })
            }
          })
        },
        fail: (res) => {
          console.log(res.errMsg)
        }
      })
    }
  },
  //点击我的订单跳转
  tomyOrder() {
    if (this.data.name == "点击授权登录") {
      this.login()
    } else {
      wx.navigateTo({
        url: "../myorder/myorder"
      })
    }
  },
  //点击建议跳转
  tosuggest() {
    wx.navigateTo({
      url: "../suggest/suggest",
      success: (res) => {
        // 通过eventChannel向被打开页面传送数据
        res.eventChannel.emit('acceptDataFromOpenerPage', {
          title:"意见建议",
          placeholder:"请写下你宝贵的意见",
          comfirm:"提交",
          value:""
        })
      }
    })
  },
})