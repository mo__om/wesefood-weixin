const app = getApp()
Page({
  data: {
    order: [],
  },
  onShow: function () {
    this.setData({
      order: app.globalData.order.reverse()
    })
    app.globalData.order.reverse()
  },
})