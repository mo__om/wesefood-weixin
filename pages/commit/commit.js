const app = getApp()
//用于获得时间
const util = require('../../utils/util.js')
const api = require("../../api/index.js")
Component({
  data: {
    beizhu: "请填写口味偏好",
    //支付框是否要弹出来
    onzhifu: false,
    menu: [],
    imgList: [{
        text: "源码网址二维码",
        name: "https://gitee.com/mo__om/westfood",
        src: "https://mis-western-restaurant.oss-cn-guangzhou.aliyuncs.com/images/2022/04/22/efbef581-4c71-4e77-894b-7226be6d66f2.png"
      },
      {
        text: "项目经理、前端设计师、工程师：",
        name: "mo_om",
        src: "https://mis-western-restaurant.oss-cn-guangzhou.aliyuncs.com/images/2022/04/24/edd8a478-d41a-4db3-aa7f-a0ea650471de.png"
      },
      {
        text: "后端架构师、工程师、运维：",
        name: "Listener",
        src: "https://mis-western-restaurant.oss-cn-guangzhou.aliyuncs.com/images/2022/04/24/2202cd16-0a8d-4fad-913b-4d4331c8d0d8.png"
      },
      {
        text: "产品经理、设计师、文档撰写者:",
        name: "再见月亮镇",
        src: "https://mis-western-restaurant.oss-cn-guangzhou.aliyuncs.com/images/2022/04/24/6c6c3755-5a4c-448f-a4ab-d3ab0d464545.png"
      },
      {
        text: "测试员、数据员:",
        name: "布吉倒",
        src: "https://mis-western-restaurant.oss-cn-guangzhou.aliyuncs.com/images/2022/04/24/749814d9-d608-470a-8261-7ed7ba0f2f71.png"
      },
    ],
    totalmuch: 0,
    //这个页面的bigscroll，不要搞错
    bigscroll: true,
    // seat:app.globalData.state.seat  因为这个会在本this还没创建时就传入，监控时动作早已经在父类发送，所以刚好错过，可以在ready时再赋值一次
  },

  // 从上一个页面获得数据
  ready() {
    const eventChannel = this.getOpenerEventChannel()
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel.on('acceptDataFromOpenerPage', (data) => {
      this.setData({
        menu: data.carthing,
        totalmuch: data.totalmuch.slice(1)
      })
    })
    // 那个选择组件改变时会触发这个
    app.makeWatcher(app.globalData, "state.seat", (val) => {
      this.setData({
        seat: val
      })
    })
    //第一次时
    this.setData({
      seat: app.globalData.state.seat
    })

   
    
    

  },

  methods: {
    beizhu() {
      wx.navigateTo({
        url: "../suggest/suggest",
        events: {
          // 为指定事件添加一个监听器，获取被打开页面传送到当前页面的数据
          beizhu: ({
            text
          }) => {
            this.setData({
              beizhu: text
            })
          }
        },
        success: (res) => {
          // 通过eventChannel向被打开页面传送数据
          res.eventChannel.emit('acceptDataFromOpenerPage', {
            title: "备注",
            value: this.data.beizhu,
            placeholder: "如：不吃香菜",
            comfirm: "确认"
          })
        }
      })
    },
    //改变桌子时
    showtable() {
      this.selectComponent(".seat").show("/user/changeTable")
    },
    //弹出支付框
    showzhifu() {
      this.setData({
        onzhifu: true,
        bigscroll: false
      }, () => {
        this.animate(".zhezhao", [{
          opacity: 0
        }, {
          opacity: 0.6
        }], 200, () => {
          this.animate(".zhifuspan", [{
            opacity: 0,
            bottom: "-700px"
          }, {
            opacity: 1,
            bottom: "200px"
          }], 300)
        })
      })
    },
    //点击遮罩关闭支付框
    shutzhifu() {
      this.animate(".zhifuspan", [{
        opacity: 1,
        bottom: "300px"
      }, {
        opacity: 0,
        bottom: "-700px"
      }], 300, () => {
        this.animate(".zhezhao", [{
          opacity: 0.6
        }, {
          opacity: 0
        }], 200, () => {
          this.setData({
            onzhifu: false,
            bigscroll: true
          })
        })
      })
    },
    //成功支付时
    successzhifu() {
      let temOrder = JSON.parse(JSON.stringify(this.data.menu))
      let temtotalAmount = JSON.parse(JSON.stringify(this.data.totalmuch))
      console.log(temOrder)
      api.commitOrder({
        data: {
          // temOrder:temOrder,
          saleTime: util.formatTime(new Date()),
          totalAmount: temtotalAmount,
          remark: this.data.beizhu === "请填写口味偏好" ? "" : this.data.beizhu
        },
        success: (res) => {
          if (res.data.code === 409) {
            wx.showModal({
              title: '提示',
              content: '该座位已被选择，等待刷新后重新选择',
              success: (res) => {
                this.selectComponent(".seat").show("/user/changeTable")
              }
            })
          } else {
            app.globalData.order.push({
              saleTime: util.formatTime(new Date()),
              order: temOrder,
              totalAmount: temtotalAmount,
              zhanshou: true,
              remark:this.data.beizhu === "请填写口味偏好" ? "" : this.data.beizhu
            })
            wx.showToast({
              title: '付款成功',
              icon: 'success',
              duration: 2000,
            })
            const eventChannel = this.getOpenerEventChannel()

            // 跳到本次订单界面
            setTimeout(() => {
              wx.switchTab({
                url: '../benorder/benorder',
              })
              //这样不会向后端要求清空购物车造成异常
              eventChannel.emit('clearall', {
                requestOrNO: false
              });
              //提交订单以后后端需要重新选桌，我们在跳到那个新界面前偷偷帮顾客把桌子选好
              this.selectComponent(".seat").shut("/user/chooseTable")
            }, 2000)
          }
        }
      })

    },
  }
})