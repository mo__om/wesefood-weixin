# 真实伤害-西餐厅点餐微信小程序

#### 介绍
    基于微信小程序框架打造的西餐厅点餐小程序

 **小程序二维码** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0526/202808_a8235273_7591984.png "屏幕截图.png")



 **页面截图** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0419/163923_15a6f3c9_7591984.png "屏幕截图.png")


#### 使用
    1.使用开发者工具(https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)打开
![输入图片说明](https://images.gitee.com/uploads/images/2022/0419/163137_61e1efff_7591984.png "屏幕截图.png")

    2.构建npm 
![输入图片说明](https://images.gitee.com/uploads/images/2022/0419/163239_68efa350_7591984.png "屏幕截图.png")

    3.编译运行

