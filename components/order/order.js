// pages/benorder/benorder.js
const app=getApp()

Component({

  properties:{
    order:Array
  },
  methods:{
    move(e){
      this.setData({
        [`order[${e.currentTarget.dataset.index}].zhanshou`]:!e.currentTarget.dataset.zhanshou
      })
    },
  }
})