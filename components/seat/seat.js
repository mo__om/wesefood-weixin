const app = getApp()
const api = require("../../api/index.js")

Component({
  data: {
    on: false,
    //是否有座位，没有的话渲染出来的是无座
    empty: false,
    //让下拉刷新结束
    triggered: false,
  },
  properties: {
    scroll: Boolean,
    title: String
  },
  methods: {
    //下拉刷新拿数据
    onRefresh() {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('myevent', myEventDetail, myEventOption)
      if (this._freshing) return
      this._freshing = true
      this.getData()
      setTimeout(() => {
        this.setData({
          triggered: false,
        })
        this._freshing = false
      }, 1200)
    },
    bindchange: function (e) {
      const i = e.detail.value[0]
      this.setData({
        index: [i]
      })
    },
    //第一次进入或者下拉刷新时都可以重新拿数据
    getData() {
      api.getTable({
        success: (res) => {
          if (res.statusCode === 500) {
            setTimeout(() => {
              this.getData()
            }, 500)
          } else {
            let seats = []
            if (res.data.data.tableList.length === 0) {
              this.setData({
                empty: false
              })
            } else {
              res.data.data.tableList.map((item) => {
                if (item.isOccupied === 0) {
                  seats.push(item.seatNo)
                }
              })
              if (seats.length === 0) {
                this.setData({
                  empty: false
                })
              } else {
                this.setData({
                  seats: seats,
                  index: [app.globalData.state.seatindex],
                  empty: true
                })
              }
            }
          }
        },
        fail: () => {
          console.log("执行了吗")
          this.getData()
        }
      })
    },
    //把窗体显示出来
    show(url) {
      // 拿数据
      this.getData()
      this.setData({
        on: true,
        scroll: false,
        url: url
      }, () => {
        this.animate(".zhezhao", [{
          opacity: 0
        }, {
          opacity: 0.6
        }], 200, () => {
          this.animate(".span", [{
            opacity: 0
          }, {
            opacity: 1
          }], 200)
        })
      })
    },
    shutShare() {
      wx.showToast({
        title: '选桌成功',
        icon: 'success',
        duration: 1500,
        mask: true,
        complete: () => {
          this.setData({
            on: false,
            scroll: true,
          })
        }
      })
      this.animate(".span", [{
        opacity: 1
      }, {
        opacity: 0
      }], 100)
      this.animate(".zhezhao", [{
        opacity: 0.6
      }, {
        opacity: 0
      }], 100)
    },
    shutShowOrNo(url,realUrl,tempSeat,tempIndex){
      let dataAndCall = {
        data: {
          table: tempSeat
        },
        success: (res) => {
          if (res.data.code === 409) {
            wx.showModal({
              title: '提示',
              content: '该座位已被选择，等待刷新后重新选择',
              success: (res) => {
                this.setData({
                  triggered: true,
                })
              }
            })
          } else if (res.statusCode === 200) {
            if (this.data.seats) {
              app.globalData.state.seat = tempSeat
              app.globalData.state.seatindex = tempIndex
            }
            if (typeof url === "object") {
              
              this.shutShare()

            }
          }
        },
      }
      if (realUrl === "/user/chooseTable") {
        api.chooseTable(dataAndCall)
      } else if (realUrl === "/user/changeTable") {
        api.changeTable(dataAndCall)
      }
    },
    //点击确定
    shut(url) {
      
      
        //有改座位和选座位和提交订单后自动调用shut三种情况，传过来的api参数是不一样的
        //当提交订单自动重新选座位时，shut会没有data,而seat如果提交失败不能被更新,所以当交订单后自动调用shut时tempSeat为上次存的全局变量，否则就是要看选了什么，而选了什么依赖于index而不依赖于seat
        let realUrl, tempSeat, tempIndex
        if (typeof url === "object") {
          wx.showLoading({
            title: '正在分配座位',
            duration: 400
          })
          setTimeout(()=>{
            realUrl = this.data.url
            tempSeat = this.data.seats[this.data.index] + ""
            tempIndex = this.data.index
            this.shutShowOrNo(url,realUrl,tempSeat,tempIndex)
          },700)  
        } else {
          realUrl = url
          tempSeat = app.globalData.state.seat
          tempIndex = app.globalData.state.seatindex
          this.shutShowOrNo(url,realUrl,tempSeat,tempIndex)
        }
        
      
    }
  }
})