//主要是针对安卓标题不居中，几乎除了点餐界面都用了这个组件，可以传入标题渲染
Component({
  data: {
    statusBarHeight:wx.getSystemInfoSync().statusBarHeight,
    navigatorHeight: wx.getMenuButtonBoundingClientRect().top,
  },
  properties:{
    title:String,
    backRequire:Boolean
  },
  methods:{
    back(){
      wx.navigateBack()
    }
  }
})