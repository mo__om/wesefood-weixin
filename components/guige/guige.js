const app = getApp()
//主要用于引入计算属性
const computedBehavior = require('miniprogram-computed').behavior
//引入访问后端api
const api = require("../../api/index.js")

Component({
  behaviors: [computedBehavior],
  data: {
    show: false,
    selenum: 1,
    // 是不是修改
    changeOrNo: false,
    //后端要修改的老descri
    oldname:"",
    item: {
      more: []
    }
  },
  properties: {
    bigscroll: Boolean,
  },

  computed: {
    //已选那行字进行计算
    selectoption(data) {
      let descri = '已选：'
      data.item.more.map(item => {
        descri += `${item.choose}+`
      })
      return descri.slice(0, descri.length - 1)
    },
    //所选选项价钱进行计算
    seleprice(data) {
      let p = data.item.price

      data.item.more.map(item => {

        p += item.addprice
      })
      return p
    },
  },

  methods: {
    //提交到购物车，包括改和增的
    addtoCar() {
      let myEventDetail = {
        item: this.data.item,
        price: this.data.seleprice,
        num: this.data.selenum,
        descir: this.data.selectoption.slice(3),
      }
      //是不是改，要特殊处理，特别是购物车部分要等等，以防购物车先增后减的画面被看到
      if (this.data.changeOrNo) {
        let fdes=this.data.selectoption.slice(3)
        let foldDes=this.data.oldname
          //传给后端的descri要经过特殊处理
          if (fdes.indexOf("+") == -1) {
            fdes = fdes + "+"
            foldDes+="+"
        }
        //调用改的借口
        api.changeCar({
          data: {
            itemNo: this.data.item.itemNo,
            price: this.data.seleprice+"",
            oldname:foldDes,
            name: fdes,
            num: this.data.selenum
          },
          success() {}
        })
        wx.showToast({
          title: '修改成功',
          icon: 'success',
          duration: 2000,
        })
        myEventDetail.change = true
        this.triggerEvent('guige', myEventDetail)
        setTimeout(() => {
          this.hide()
        }, 10)
      } else {
        // 普通的增加
        if (this.data.item.combo === false && this.data.item.guige === true) {
          let fdes=this.data.selectoption.slice(3)
          if (fdes.indexOf("+") == -1) {
            fdes = fdes + "+"
          }
          api.dofood({
            data: {
              itemNo: this.data.item.itemNo,
              method: "add",
              detail: fdes,
              num: this.data.selenum
            }
          })
        } else if (this.data.item.combo === true && this.data.item.guige === false) {
          api.doCombo({
            data: {
              comboNo: this.data.item.itemNo,
              method: "add",
              name: this.data.selectoption.slice(3),
              num: this.data.selenum,
              price:this.data.seleprice+""
            },
            success() {
            }
          })
        }
        //针对的是普通增加
        this.hide()
        this.triggerEvent('guigeAnimate', myEventDetail)
      }
      //把是不是修改和num改回来
      setTimeout(() => {
        this.setData({
          selenum: 1,
          changeOrNo: false
        })
      }, 0)
    },
    //内部的num增减
    add() {
      this.setData({
        selenum: this.data.selenum + 1
      })
    },
    jian() {
      if (this.data.selenum > 1) {
        this.setData({
          selenum: this.data.selenum - 1
        })
      }
    },
    //动画
    showOrChange(call) {
      if (this.data.show === false) {
        this.setData({
          show: true,
          bigscroll: false
        })
        this.animate('.outter', [{
            opacity: 0.9,
            bottom: -330
          },
          {
            opacity: 1.0,
            bottom: 0
          },
        ], 90)
        this.animate('#zhezhao', [{
            opacity: 0
          },
          {
            opacity: 0.6
          }
        ], 90)
        call()
      }
    },
    //把要渲染的item注入
    show(i) {
      this.showOrChange(() => {
        this.setData({
          item: i
        })
      })
    },
    //改
    change(father, son) {
      const num = son.num
      father.kids = []
      // 这里是修改，所以要把原来的选择存起来，还有选的数量，相当于复杂版show，之后就用show的增加流程相当于新增进购物车，那边再把原来的删掉
      this.showOrChange(() => {
        this.setData({
          item: father,
          oldname:son.descir,
          selenum: num,
          changeOrNo: true
        })
      })
      
    },
    hide(e) {
      if (!this.data.changeOrNo || typeof e !== "object") {
        this.setData({
          show: false,
          bigscroll: true
        })
        this.animate('.outter', [{
          opacity: 0,
          bottom: -330
        }, ], 1)
      }
    },
    // 选
    pick(e) {
      //基本思想是点到哪个东西就把这个东西的name和加价赋值到这个option这里，然后总的price再去把每个option相加，同时渲染时每个option组看一下自己name和父选项对应name，对应上了就变黑被选中
      const pick = e.currentTarget.dataset.pick
      const pchoose = `item.more[${pick.parentIndex}].choose`
      const paddprice = `item.more[${pick.parentIndex}].addprice`
      const paddItem = `item.more[${pick.parentIndex}].addItem`
      this.setData({
        [pchoose]: pick.name,
        [paddprice]: pick.addprice,
        [paddItem]: pick.itemNo
      })
    }
  }
})