// 包装一下wx.request不必重复写一些东西
const defaultHeader={
  'Content-Type': "application/json",
  'Authorization':wx.getStorageSync('token')
}
function axiwx({url,method="POST",data="",header=defaultHeader,successCall,failCall}){
  console.log(url+"发送为下面数据")
  console.log(data)
  let timer=setTimeout(()=>{
    wx.showLoading({
      title: 'Real 西餐厅',
    })
  },300)
  wx.request({
    url: 'https://miswestrestaurant.xyz'+url,
    method:method,
    data:data,
    header:header,
    success:(res)=>{
      clearTimeout(timer)
      wx.hideLoading()
      successCall&&successCall(res)
      console.log(url+"返回为下面数据")
      console.log(res)
    },
    fail:(res)=>{
      console.log(url+"返回为下面数据")
      console.log(res)
      failCall&&failCall(res)
      wx.hideLoading()
      wx.showToast({
        title: '请检查网络重试',
        icon: 'error',
        duration: 4000
      })
    }
  })
}

const Api = {
  staticLogin:"/user/static-login",
  getTable: "/user/all-table",
  chooseTable: "/user/table",
  changeTable: "/user/changeTable",
  leftmenu:"/user/leftmenu",
  rightmenu:"/user/rightmenu",
  commitOrder:"/user/commitOrder",
  dofood:"/user/dofood",
  clearShopCar:"/user/clearShopCar",
  login:"/user/login",
  getMyOrder:"/user/getMyOrder",
  doCombo:"/user/doCombo",
  changeCar:"/user/changeCar"
}

function staticLogin({data,success}){
  axiwx({
    url: Api.staticLogin,
    method: "POST",
    header: {
      'Content-Type': "application/x-www-form-urlencoded",
    },
    data:data,
    successCall:(res)=>{
      success(res)
    }
  })
}

function getTable({success,fail}) {
  axiwx({
    url: Api.getTable,
    method: "GET",
    successCall:(res)=>{
      success(res)
    },
    failCall:(res)=>{
      fail(res)
    }
  })
}

function chooseTable({data,success}){
  axiwx({
    url: Api.chooseTable,
    method: "POST",
    data:data,
    header:{
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
      'Authorization':wx.getStorageSync('token')
    },
    successCall:(res)=>{
      success(res)
    }
  })
}

function changeTable({data,success}){
  axiwx({
    url: Api.changeTable,
    method: "GET",
    data:data,
    header:{
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
      'Authorization':wx.getStorageSync('token')
    },
    successCall:(res)=>{
      success(res)
    }
  })
}

function commitOrder({data,success}){
  axiwx({
    url:Api.commitOrder,
    method:"POST",
    data:data,
    header:{
      'Authorization':wx.getStorageSync('token')
    },
    successCall:(res)=>{
      success(res)
    }
  })
}

function leftmenu({success,fail}){
  console.log("this是谁")
  console.log(this)
  axiwx({
    url:Api.leftmenu,
    method:"GET",
    successCall:(res)=>{
      success(res)
    },
    failCall:(res)=>{
      fail(res)
    }
  })
}

function rightmenu({success,fail}){
  axiwx({
    url:Api.rightmenu,
    method:"GET",
    successCall:(res)=>{
      success(res)
    },
    failCall:(res)=>{
      fail(res)
    }
  })
}

function dofood({data,success}){
  axiwx({
    url:Api.dofood,
    method:"POST",
    data:data,
    successCall:(res)=>{
      success&&success(res)
    }
  })
}

function clearShopCar({success}){
  axiwx({
    url:Api.clearShopCar,
    method:"GET",
    successCall:(res)=>{
      success&&success(res)
    }
  })
}

function login({data,success}){
  axiwx({
    url:Api.login,
    method:"POST",
    data:data,
    successCall:(res)=>{
      success()
    }
  })
}

function getMyOrder({success}){
  axiwx({
    url:Api.getMyOrder,
    method:"GET",
    successCall:(res)=>{
      success(res)
    }
  })
}

function doCombo({data,success}){
  axiwx({
    url:Api.doCombo,
    method:"POST",
    data:data,
    successCall:(res)=>{
      success(res)
    }
  })
}

function changeCar({data,success}){
  axiwx({
    url:Api.changeCar,
    method:"POST",
    data:data,
    successCall:(res)=>{
      success(res)
    }
  })
}

module.exports = {
  staticLogin,
  getTable,
  chooseTable,
  changeTable,
  commitOrder,
  leftmenu,
  rightmenu,
  dofood,
  clearShopCar,
  login,
  getMyOrder,
  doCombo,
  changeCar,
  defaultHeader
}